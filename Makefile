all :

up:
	# git
	@ if [[ -d .git ]]; then git add . ; fi
	@ if [[ -d .git ]]; then git commit ; fi
	@ if [[ -d .git ]]; then git push ; fi
	# mercurial
	@ if [[ -d .hg ]]; then hg addremove ; fi
	@ if [[ -d .hg ]]; then hg commit ; fi
	@ if [[ -d .hg ]]; then hg push ; fi



.PHONY: up