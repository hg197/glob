#include <glob.h>
#include <string>
#include <vector>


#ifndef GLOB
#define GLOB

void file_list(const char * pattern, std::vector<std::string> & list = std::vector() ) ;

void file_list(const std::string & pattern, std::vector<std::string> & list = std::vector() ) ;

#endif