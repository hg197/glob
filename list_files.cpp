#include <list_files.h>

void file_list(const char * pattern, std::vector<std::string> & list)
{
	glob_t res ;

	glob(pattern,GLOB_TILDE,NULL,&res) ;

	list.reserve(res.gl_pathc) ;

	for(register int i = 0; i < res.gl_pathc; i++)
	{
		list.push_back(res.gl_pathv[i]) ;
	}

	globfree(&res) ;

return ;
}

void file_list(const std::string & pattern, std::vector<std::string> & list)
{
	glob_t res ;

	glob(pattern.c_str(),GLOB_TILDE,NULL,&res) ;

	list.reserve(res.gl_pathc) ;

	for(register int i = 0; i < res.gl_pathc; i++)
	{
		list.push_back(res.gl_pathv[i]) ;
	}

	globfree(&res) ;

return ;
}